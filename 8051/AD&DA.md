---
aliases: 
tags:
  - 51单片机
  - c
summary: 
create time: 2023-12-25T15:26:00
modify time:
---
## Introduction

### ADC（analog to digital converter）

也称为模数转换器，是指一个将模拟信号转变为数字信号。单片机在采集模拟信号时，通常都需要在前端加上 A/D 芯片。

### DAC（Digital to analog converter）

即数字模拟转换器，它可以将数字信号转换为模拟信号。它的功能与 ADC 相反。在常见的数字信号系统中，大部分传感器信号被转化成电压信号，而 ADC 把电压模拟信号转换成易于计算机存储、处理的数字编码，由计算机处理完成后，再由 DAC 输出电压模拟信号，该电压模拟信号常常用来驱动某些执行器件，使人类易于感知。

### 硬件电路

#### AD

![[Pasted image 20231225194942.png]]

![[Pasted image 20231225200056.png]]

#### DA

![[Pasted image 20231225195008.png]]

![[Pasted image 20231225200112.png]]

1. AD转化通常有多个输入通道，用多路选择器连接至AD转换器，以实现AD多路复用的目的，提高硬件利用率。
2. AD/DA模块与单片机数据传输，可以使用并口（速度快，原理简单），也可以使用串口（接线少，使用方便）。
3. 可以将AD/DA模块直接集成在单片机内部，直接写入/读出寄存器就可以进行AD/DA转换，单片机IO口可以直接复用为AD/DA的通道。
4. AD应用范围大，DA应用范围小（可以使用PWM代替）

### 运算放大器

运算放大器（简称“运放”）是具有很高放大倍数的电路单元。

内部集成了==差分放大器==、==电压放大器==、==功率放大器==三级放大电路。

运算放大器可构成的电路有：电压比较器、反相放大器、同相放大器、电压跟随器、加法器、积分器、微分器等。

运算放大器电路的分析方法：虚短，虚断（负反馈条件下）

![[Pasted image 20231225202521.png]]

#### 电压比较器

![[Pasted image 20231225205121.png]]

#### 反向放大器

![[Pasted image 20231225205202.png]]

#### 同相放大器

![[Pasted image 20231225210125.png]]

#### 电压跟随器

![[Pasted image 20231225210339.png]]

### DA原理

#### T型电阻网络DA转换器

![[Pasted image 20231225211527.png]]

#### PWM型DA转换器

![[Pasted image 20231225211642.png]]

### AD原理

#### 逐次逼近型AD转换器

![[Pasted image 20231225212534.png]]

### AD/DA性能指标

#### 分辨率

指AD/DA数字量的精细程度，通常用位数表示，位数越高，分辨率越高。

#### 转换速度

表示AD/DA的最大采样/建立频率，通常用转换频率或转换时间表示。

## XPT2046

[xpt2046中文](./attachments/xpt2046中文)

XPT2046 是一款 4 线制电阻式触摸屏控制器，内含 12 位分辨率 125KHz 转换速率逐步逼近型 A/D 转换器。

### XPT2046通信时序

==SPI通信==

![[Pasted image 20231225232116.png]]

#### XPT2046控制字

![[Pasted image 20231226205916.png]]

#### 通道选择位

![[Pasted image 20231226210616.png]]

#### 差分模式选择位 

![[Pasted image 20231226210642.png]]

## 代码

### XPT2046.h

```c
#ifndef __XPT2046_H__
#define __XPT2046_H__

#define XPT2046_XP_8    0x9C
#define XPT2046_YP_8    0xDC
#define XPT2046_VBAT_8  0xAC
#define XPT2046_AUX_8   0xEC

#define XPT2046_XP_12    0x94
#define XPT2046_YP_12    0xD4
#define XPT2046_VBAT_12  0xA4
#define XPT2046_AUX_12   0xE4

unsigned int XPT2046_ReadAD(unsigned char Command);

#endif // __XPT2046_H__
```

### XPT2046.c

```c
#include <REGX52.H>

sbit XPT2046_CS = P3^5;
sbit XPT2046_DCLK = P3^6;
sbit XPT2046_DIN = P3^4;
sbit XPT2046_DOUT = P3^7;

/**
 * @brief 读取XPT2046模拟量转化为数字量的结果
 * 
 * @param Command 控制字：选择写入模拟量通道 
 * @return unsigned int 模拟量转化结果（数字量）
 * @note SPI总线传输协议，高位在前
 */
unsigned int XPT2046_ReadAD(unsigned char Command) {
  unsigned int ADValue = 0;
  unsigned char i;
  // 开始数据传输
  XPT2046_CS = 0;
  XPT2046_DCLK = 0;

  // 向XPT2046写控制字（8位）
  for (i = 0; i < 8; ++i) {
    XPT2046_DIN = Command & (0x80 >> i);
    XPT2046_DCLK = 1;
    XPT2046_DCLK = 0;
  }
  // XPT2046 BUSY STC89C52可省略
  // 读取XPT2046转化结果（16位数字量）
  for (i = 0; i < 16; ++i) {
    XPT2046_DCLK = 1;
    XPT2046_DCLK = 0;
    if (XPT2046_DOUT) {
      ADValue |= (0x8000 >> i); 
    }
  }

  // 结束数据传输
  XPT2046_CS = 1;

  if (Command & 0x08) {
    return (ADValue >> 8);
  } else {
    return (ADValue >> 4);
  }
}
```

### AD 数模转换

```c
#include <REGX52.H>
#include "Delay.h"
#include "LCD1602.h"
#include "XPT2046.h"

unsigned int ADValue;

void main() {
  LCD_Init();
  LCD_ShowString(1, 1, "ADJ  NTC  RG");
  while (1) {
    ADValue = XPT2046_ReadAD(XPT2046_XP_8);
    LCD_ShowNum(2, 1, ADValue, 3);
    ADValue = XPT2046_ReadAD(XPT2046_YP_8);
    LCD_ShowNum(2, 6, ADValue, 3);
    ADValue = XPT2046_ReadAD(XPT2046_VBAT_8);
    LCD_ShowNum(2, 11, ADValue, 3);
    Delay(10);
  }
}
```

### DA 模数转换

```c
#include <REGX52.H>
#include "Delay.h"
#include "Timer0.h"
#include "Nixie.h"
#include "Key.h"

sbit DA = P2^1;
sbit LED = P2^0;

unsigned char Counter, Compare, Speed;
unsigned char i;

void main() {
  Timer0_Init();
  while (1) {
    for (i = 0; i < 100; ++i) {
      Compare = i;
      Delay(10);
    }
    for (i = 100; i > 0; --i) {
      Compare = i;
      Delay(10);
    }
  }
}

/**
 * @brief 每隔100us进入一次中断处理函数 
 *  
 */
void Timer0_Routine() interrupt 1 {
	TL0 = 0x9C;		//设置定时初值
	TH0 = 0xFF;		//设置定时初值
  ++Counter;
  Counter %= 100;
  if (Counter < Compare) {
    DA = 1; // 电机工作
  } else {
    DA = 0; // 电机不工作
  }
}
```
