---
aliases: 
tags:
  - graphics
  - vulkan
summary: 
create time: 2023-12-30
modify time:
---
## GPU programming 

Shader (作色器程序)：在 GPU 上运行的程序，编程语言 GLSL、HLSL、CG。

## CPU 和 GPU 交互

### GPU 向 CPU 提供的 API 规范种类

1. OpenGl
2. Vulkan
3. DirectX
4. Metal
