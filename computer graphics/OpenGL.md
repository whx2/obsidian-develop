---
aliases: []
tags:
  - c
  - graphics
  - opengl
summary: 
create time: 2023-12-30
modify time:
---
## Introduction

![[Pasted image 20231230155753.png]]

[wiki](https://en.wikipedia.org/wiki/OpenGL)

OpenGL (Open Graphics Library)，一个由 Khronos 组织制定并维护的规范的（Specification）的 API，同时 OpenGL 指代其具体实现，是一个 C 库。\

### 核心模式（core-profile）

也叫可编程管线，提供了更多的灵活性，更高的效率，更重要的是可以更深入理解图形编程。

### 立即渲染模式（Immediate mode）

- 早期的 OpenGL 使用的模式（固定渲染管线）
- OpenGL 的大多数功能都被库隐藏起来，容易使用和理解，执行效率过低
- 开发者很少能控制 OpenGL 进行计算
- OpenGL 3.2 开始，引入核心模式

### 状态机（State Machine）

- OpenGL 自身是一个巨大的状态机，描述操作和控制所有变量的大集合
- OpenGL 的状态通常被称为上下文（context）
- 状态设置函数（State-changing Function）
- 状态应用函数（State-using Function）

> As long as you keep in mind that OpenGL is basically one large state machine, most of its functionality will make more sense.

我们通过改变一些上下文变量来改变 OpenGL 状态，从而告诉 OpenGL 如何绘图。

### OpenGL 对象

一个对象是指一些选项的集合，代表 OpenGL 状态的一个子集。