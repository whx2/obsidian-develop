---
aliases: 
tags:
  - 编译原理
  - 语法分析
summary: 
create time: 2024-05-30
modify time:
---
## 表驱动的 LR 分析器架构

![[Pasted image 20240530230332.png]]

## LR(0) 分析算法

![[Pasted image 20240530230435.png]]

### LR(0)分析算法的缺点

![[Pasted image 20240530230559.png]]

### 缺点一：错误定位

![[Pasted image 20240530230946.png]]

### 缺点二：冲突

![[Pasted image 20240530231128.png]]

## SLR分析算法

![[Pasted image 20240530231234.png]]

### SLR分析算法示例

