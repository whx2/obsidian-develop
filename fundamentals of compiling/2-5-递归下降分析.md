---
aliases: 
tags:
  - 编译原理
  - 语法分析
summary: 
create time: 2024-05-28
modify time:
---
递归下降算法是自顶向下分析算法中的一类，手工实现语法分析器。

基于分治法。

![[Pasted image 20240528135035.png]]

## 算法实现

![[Pasted image 20240528135402.png]]

### 一般的算法框架

通常还需要计算First集和Follow集。

![[Pasted image 20240528140648.png]]

## 算术表达式的递归下降分析示例

无二义性上下文无关文法（已经处理 + 和 * 运算优先级）

存在需要回溯的情况

![[Pasted image 20240528142153.png]]

利用文法的性质和特性避免回溯的情况

![[Pasted image 20240528141534.png]]