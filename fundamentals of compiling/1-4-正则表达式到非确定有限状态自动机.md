---
aliases: 
tags:
  - 编译原理
  - 词法分析
summary: 
create time: 2024-05-27
modify time:
---
## 自动生成词法分析器流程

![[Pasted image 20240527204023.png]]

## Thompson 算法 （RE -> NFA）

![[Pasted image 20240527222558.png]]

![[Pasted image 20240527222958.png]]

![[Pasted image 20240527223156.png]]

### Thompson 算法转化示例

递归构造

![[Pasted image 20240527223544.png]]

![[Pasted image 20240527224014.png]]

## 子集构造算法（NFA -> DFA）

NFA 状态转移不确定，不适合词法分析器识别。

![[Pasted image 20240527225427.png]]

![[Pasted image 20240527225932.png]]

### 1. 求在x字符驱动下转化状态的集合

### 2. 对转化状态集合中的每个元素求 $\varepsilon$ 闭包运算

#### $\varepsilon$ 闭包运算深度优先实现

![[Pasted image 20240527230043.png]]

#### $\varepsilon$ 闭包运算广度优先实现

![[Pasted image 20240527230101.png]]

### 子集构造算法转化示例

Q：最终结果DFA

![[Pasted image 20240527230803.png]]

### 子集构造算法实现

![[Pasted image 20240527231246.png]]

## Hopcroft 最小化算法 （最小化DFA）

最小化DFA，提高算法效率

![[Pasted image 20240528111055.png]]

### 算法实现

![[Pasted image 20240528111735.png]]

### 使用Hopcroft最小化算法DFA示例

#### 示例1

![[Pasted image 20240528111845.png]]

#### 示例2

![[Pasted image 20240528112317.png]]

## DFA 代码表示（DFA -> 词法分析器代码）

DFA 数据结构为一个有向图。

DFA 代码实现方式：

1. 转移表（类似于邻接矩阵）
2. 哈希表
3. 跳转表
4. ...

### 转移表实现DFA

#### 转移表

![[Pasted image 20240528113618.png]]

#### 词法分析驱动

![[Pasted image 20240528114340.png]]

#### 最长匹配

具有相同前缀的关键字或标识符，遵循最长匹配规则

![[Pasted image 20240528114842.png]]

## 跳转表实现DFA

将每一种状态实现为一段代码。状态之间边的转移实现为一次状态的跳转。不需要维护状态转移表的数组。

![[Pasted image 20240528115648.png]]