---
aliases: []
tags:
  - stm32
summary: 
create time: 2024-02-25
modify time: 2024-02-25
---
# 开发环境搭建

## Windows

### 安装 Keil 5 MDK

### 安装器件支持包

芯片类型多，不同类型芯片的支持包不同，开发某种芯片，下载对应支持包

### 软件注册

### 安装 STLINK 驱动

### 安装 USB 转串口驱动

# 项目工程建立

## 新建工程步骤

1. 新建工程、选择型号
2. Start（启动程序的汇编文件、源文件、头文件）、Library（封装库的源文件和头文件）、User（库函数头文件配置文件、库函数中断程序配置文件、用户程序文件） 文件夹
3. Keil 5 配置文件查找路径
4. 配置宏定义
5. 选择调试器并设置为 Flash Download 中 Reset and Run（下载并运行）


## 工程架构

### 用户文件 User

![[Pasted image 20240225200927.png]]

#### 用户主程序`main.c`

#### 头文件包含相关的配置文件`stm32f10x_conf.h`

库函数配置、库函数头文件包含

#### 系统和用户中断处理函数文件`stm32f10x_it.c\.h`

定义用户和系统中断处理函数

### 启动文件夹 Start

![[Pasted image 20240225200115.png]]

#### 启动文件 `start/start_xx.s`

定义中断向量表、中断服务函数（包含复位中断：调用 SystemInit (在 `start/system_xx.c`) 和 main）等。

#### 系统初始化函数文件`system_xx.c\.h`

初始化嵌入式 Flash 接口、锁相环、更新系统内核的时钟变量

#### 外设寄存器描述`stm32f10x.h`

#### 内核寄存器描述`core_cm3.c\.h`

### 库函数文件夹 Library

![[Pasted image 20231229160630.png]]




