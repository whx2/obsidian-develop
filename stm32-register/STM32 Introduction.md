---
aliases: 
tags:
  - stm32
  - arm
  - Cortex-M
summary: 
create time: 2023-12-29
modify time: 2024-02-25
---
## STM32 MCUs 32-bit Arm Cortex-M

STM 32 是 ST 公司基于 ARM Cortex-M 内核开发的 32-bit MCU

![[Pasted image 20240225161953.png]]

CoreMark：内核跑分

## ARM Introduction

- ARM：ARM 公司，ARM 指令集，ARM 处理器内核
- ARM 公司是全球领先的半导体知识产权（IP）提供商
- ARM 公司设计 ARM 内核，半导体厂商完善内核周边电路并生产芯片

![[Pasted image 20240225162236.png]]

### 型号

![[Pasted image 20240225162349.png]]

## STM32F103C8T6

- 系列：主流系列 STM32F1
- 内核：ARM Cortex-M3
- 主频：72 MHz
- RAM：20 K（SRAM）
- ROM：64 K（Flash）
- 供电：2.0～3.6 V（标准 3.3 V，STC89C51：5 V）
- 封装：LQFP48

### 片上资源/外设（Peripheral）

![[Pasted image 20231229134053.png]]

### 命名规则

### 系统结构

### 引脚定义

#### 程序下载方式（硬件及电路）

1. ST-LINK
2. J-LINK
3. USB 转 TTL，串口（无法进行程序仿真，STC89C52 系列单片机使用）

### 程序仿真调试（协议）

1. JTAG
2. SWD

### 程序仿真器

1. J-Link，支持 SWD 和 JTAG
2. U-Link
3. ST-Link，支持 SWD

### 启动配置

BOOT 0 和 BOOT 1 以引脚功能，指定程序开始运行的位置

![[Pasted image 20231229142558.png]]

#### 系统存储器

存储 STM32 BootLoader 程序（接受串口数据，存储到主 flash 中，实现使用串口下载程序）

### 最小系统电路

![[Pasted image 20231229143138.png]]

## 开发方式

1. 基于寄存器（8051 开发方式）：程序直接配置寄存器
2. 基于标准库：使用 ST 提供封装好的标准库函数间接配置寄存器
3. 基于 HAL 库：图形化界面配置寄存器

## 工程架构

![[Pasted image 20231229160630.png]]