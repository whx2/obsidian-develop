---
aliases: 
tags:
  - stm32
  - GPIO
summary: 
create time: 2024-02-27
modify time: 2024-02-27
---
## GPIO (General Purpose Input Output)

软件可控制的引脚，stm32芯片的GPIO引脚与外部设备连接起来，实现与外部通信、控制、数据采集的功能。

![[Pasted image 20240227162210.png]]

## STM32F10X 系列引脚分类

![[Pasted image 20240227162903.png]]

## GPIO 结构

![[Pasted image 20240227163031.png]]

### 保护二极管模块

### 推挽、开漏或关闭模块

#### 推挽输出

可输出低电平0，高电平1

![[Pasted image 20240227163722.png]]

![[Pasted image 20240227164606.png]]

#### 开漏输出

仅能输出低电平0，==输入高电平1时，无外加上拉电阻时，输出为高阻态，外加上拉电阻后，可以输出高电平1==（高电平1驱动能力较弱）

![[Pasted image 20240227163949.png]]

![[Pasted image 20240227164655.png]]

### 输出寄存器 ODR (output data register)

### 位设置/清除寄存器 BSRR (bit set reset register)

### 复用功能输出（来自片上外设）

### 输入寄存器 IDR (input data register)

### 斯密特触发器

将模拟量转化为数字量

TTL电平标准

| 模式  |   L   |   H   |
| :-: | :---: | :---: |
| 输入  | <1.2V | >2.0V |
| 输出  | <0.8V | >2.4V |

### 复用功能输出（输出到片上外设）

### 模拟输入

## GPIO 初始化顺序

![[Pasted image 20240227171152.png]]

## 使用寄存器映射点亮LED

### 自定义stm32f10x.h

```c
// 实现stm32寄存器映射

// 外设 perirhral

#define PERIPH_BASE     ((unsigned int)0x40000000)
#define APB1PERIPH_BASE (PERIPH_BASE)
#define APB2PERIPH_BASE (PERIPH_BASE + 0x10000)
#define AHPPERIPH_BASE  (PERIPH_BASE + 0x20000)

#define RCC_BASE        (AHPPERIPH_BASE + 0x1000)
#define GPIOB_BASE      (APB2PHRIPH_BASE + 0x0C00)

#define RCC_APB2ENR     *(unsigned int *)(RCC_BASE + 0x18)

#define GPIOB_CRL       *(unsigned int *)(GPIOB_BASE + 0x00)
#define GPIOB_CRH       *(unsigned int *)(GPIOB_BASE + 0x04)
#define GPIOB_ODR       *(unsigned int *)(GPIOB_BASE + 0x0C)
```

### main.c

```c
# if 0

#include <REG52.H>

sbit LED = P0^0

void main(void) {

	P0 = 0xFE;	// register operation
	
	LED = 0;		// bit operation
	
}

#endif

#include "stm32f10x.h"

int main(void) {
#if 0 
	// 开启 GPIOB 的时钟
	*(unsigned int *)0x40021018 |= ( (1) << 3);
	// 配置 IO 口为输出模式，同时设置多个位前需要提前清零，防止原始数据干扰
	*(unsigned int *)0x40010C00 &= ~( (1) << (4 * 0) );
	*(unsigned int *)0x40010C00 |= ( (1) << (4 * 0) );
	// 控制 ODR 寄存器
	*(unsigned int *)0x40010C0C &= ~(1<<0);
#else
	// 开启 GPIOB 的时钟
	RCC_APB2ENR |= ( (1) << 3);
	// 配置 IO 口为输出模式，同时设置多个位前需要提前清零，防止原始数据干扰
	*(unsigned int *)0x40010C00 &= ~( (1) << (4 * 0) );
	GPIOB_CRL |= ( (1) << (4 * 0) );
	// 控制 ODR 寄存器
	GPIOB_ODR &= ~(1<<0);
	// GPIOB_ODR |= (1<<0);
#endif
}

/** 
 * 置1：|= 
 * 置0：&= ~
 */


// SystemInit 函数配置系统时钟为72MHz
// SystemInit 函数空实现，使用内部晶振HSI（8MHz）驱动外设工作
void SystemInit(void) {
	// defined symbol SystemInit, avoid compiler errors
}
```