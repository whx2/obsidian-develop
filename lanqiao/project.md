---
aliases: 
tags:
  - stm32
summary: 
create time: 2024-04-10
modify time: 2024-04-10
---
## STM32CubeMX

### Pinout & Configuration

1. 选择芯片：STM32G431RBT6
2. RCC使能外部时钟：High Speed Clock(HSE) ：Crystal/Ceramic Resonator
3. SYS模式程序下载模式配置：Debug：Serial Wire

### Clock Configuration

1. input frequency(外部时钟频率) : 24MHz (开发板外部晶振频率)
2. PLL Source Mux(锁向环输入源) : HSE
3. System Clock Mux(系统时钟源) : PLLCLK
4. HCLK : 80MHz

### Project Manager

#### Project 

1. name
2. location
3. Toolchain/IDE : MDK-ARM
4. firmware location

#### Code Generator

1. Generated files : Generate peripheral initialization as a pair of '.c/.h' file per peripheral

## MDK-ARM

1. Debug : CMSIS-DAP Debugger
2. Setting : SW
3. Flash Download : Reset and Run