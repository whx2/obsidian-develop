---
aliases: 
tags:
  - stm32
summary: 
create time: 2024-04-10
modify time: 2024-04-10
---
> 代码写在 BEGIN 和 END 之间，使用STM32CubeMX 生成代码时刷新

## LED

1. PD2 : 1 锁存器输入开启
2. PC8 ~ PC15 
	1. 0 : LED on
	2. 1:  LED off
3. PD2 : 
	1. 0 : 锁存器输入关闭
	2. 1 : 锁存器输入开启

### LED code 

```c
void led_show(uint8_t led, uint8_t mode) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
	if (mode) {
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8 << (led - 1), GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8 << (led - 1), GPIO_PIN_SET);	
	}
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_RESET);
}

```

## Key

```c
uint8_t B1_state;
uint8_t B1_last_state;
uint8_t B2_state;
uint8_t B2_last_state;
uint8_t B3_state;
uint8_t B3_last_state;
uint8_t B4_state;
uint8_t B4_last_state;

void key_scan() {
	B1_state = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0);
	B2_state = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1);
	B3_state = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2);
	B4_state = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0);
	if (B1_state == 0 && B1_last_state == 1) {
		led_show(1, 1);
	}
	if (B2_state == 0 && B2_last_state == 1) {
		led_show(1, 0);
	}
	if (B3_state == 0 && B3_last_state == 1) {
		led_show(2, 1);
	}
	if (B4_state == 0 && B4_last_state == 1) {
		led_show(2, 0);
	}
	B1_last_state = B1_state;
	B2_last_state = B2_state;
	B3_last_state = B3_state;
	B4_last_state = B4_state;
}
```

## LCD

```c
int count = 0;

char text[20];

void lcd_show() {
	sprintf(text, "        test        ");
	LCD_DisplayStringLine(Line0, (uint8_t *)text);
	sprintf(text, "     count : %d     ", count);
	LCD_DisplayStringLine(Line3, (uint8_t *)text);
}
```

## LCD 与 LED 引脚冲突

1. 使用 LCD 前关闭 LED 锁存器输入（PD2：0）
2. LCD 函数使用 `uint16_t temp = GPIOC->ODR` 和 `GPIOC->ODR = temp` 包裹。
	1. `uint16_t temp = GPIOC->ODR` 使用 temp 暂存 GPIOC 输出数据寄存器值。
	2. `GPIOC->ODR = temp` 恢复GPIOC 数据寄存器值。

## Timer

### STM32cubeMX 配置

1. Clock Source（时钟源） : Internal Clock
2. 参数设置($f_{system}=80MHz$)
	1. Prescaler (16 bits value) (PSC) (预分频值) 
	2. Counter Period (AutoReload Register) (ARR) (从装载值) 

$$ 定时器时钟频率 f = \frac{f_{system}}{(ARR + 1)(PSC + 1)} $$
$$ 定时器中断周期 T = \frac{1}{f} = \frac{(ARR + 1)(PSC + 1)}{f_{system}} $$
3. NVIC Settings：enabled（中断使能）

### code

1. 主函数 `main.c` 初始化代码中 `HAL_TIM_Base_Start_IT(TIM_HandleTypeDef *htim);` 定时器中断使能。
2. 中断回调函数（中断函数）声明 `stm32g4xx_hal_tim.h` 中 `void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);`

```c

```

## Timer and Key

1. 主函数 `main.c` 初始化代码中 `HAL_TIM_Base_Start(TIM_HandleTypeDef *htim);` ， 定时器使能。
2. 使用 `TIMx->CNT` 操作定时器计数寄存器 

## PWM

### STM32CubeMX 配置

1. 选择对应引脚设置 `PXx` 为对应定时器 `TIMx` 的对应通道 `CHx` 
2. 对应定时器 `TIMx` 的对应通道 `Channelx` ：PWM Generation CHx
3. 配置输出频率（`ARR`、`PSC`）和占空比`CCR`
	1. `ARR` `PSC` 配置输出频率(1000Hz)同定时器和计数器
		1. `ARR` : 100 - 1
		2. `PSC` : 800 - 1
	2. `CCR` ：输出比较寄存器
		1. `CNT` < `CCR` : 输出1
		2. `CNT` > `CCR` : 输出0
$$占空比 = \frac{CCR}{ARR + 1}$$

## Input Capture

$$ 时钟周期t_0 = \frac{1}{\frac{f_{system}}{PSC + 1}} = \frac{PSC + 1}{f_{system}}$$
$$捕获输入PWM周期T = t_0 * capture\_value$$
$$捕获频率f=\frac{1}{T}=\frac{f_{system}}{(PSC + 1) * capture\_value}$$
1. 选择对应捕获端口PXx的对应定时器TIMx的对应通道CHx
2. TIMx激活并设置CHx ：Input Capture direct mode
3. `PSC` ：80-1
4. NVIC Interrupt Enable

## ADC

1. `PB15` `PB12` 配置为ADC输入模式 `ACDx_INxx`
2. `ADCx` 配置为对应端口配置为 `single-ended`
3. 电压计算
	1. VDD : 0 ~ 3.3V
	2. ADC端口读取值 : 0 ~ 4096 ($2^{12}$)
$$
VOL = \frac{3.3 * adc\_value}{4096}
$$

### code

```c
double get_vol(ADC_HandleTypeDef *hadc) {
	HAL_ADC_Start(hadc);
	uint32_t adc_value = HAL_ADC_GetValue(hadc);
	return 3.3 * adc_value / 4096;
}
```

## USART

1. USARTx mode : `Asynchronous` （异步）
2. GPIO Settings
	1. PAx : `USART_TX`
	2. PAx : `USART_RX`
3.  NVIC Setting : `enable`