---
aliases: 
tags:
  - risc-v
  - os
summary: 
create time: 2024-05-11
modify time:
---
## NS16550a 编程接口介绍

RISC-V 开发板 UART 串口外设。

Hart 访问外设配置寄存器——内存映射

![[Pasted image 20240511200717.png]]

![[Pasted image 20240511200807.png]]

### NS16550a 寄存器功能

A2，A1，A0表示寄存器编号，寄存器大小为8bits，部分寄存器可功能复用。

![[Pasted image 20240511201312.png]]

### 初始化设置

![[Pasted image 20240511204102.png]]

### 数据读写

对数据的 TX/RX 有两种处理方式：

1. 轮询处理方式
2. 中断处理方式

#### 轮询处理发送数据

![[Pasted image 20240511204138.png]]