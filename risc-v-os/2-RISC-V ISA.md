---
aliases: 
tags:
  - risc-v
  - os
summary: 
create time: 2024-05-06
modify time:
---
## 指令集分层设计

### 特权级别（Privileged Level）

![[Pasted image 20240507181618.png]]

### CSR (Control and Status Registers)

![[Pasted image 20240507182052.png]]

## 指令集模块化

### 指令集命名格式

![[Pasted image 20240507175906.png]]

### 增量ISA

![[Pasted image 20240507180045.png]]

### 模块化ISA

![[Pasted image 20240507180232.png]]

![[Pasted image 20240507180707.png]]

## 通用寄存器（General Purpose Registers）

![[Pasted image 20240507180842.png]]

## HART (Hardware Thread) 硬件线程

![[Pasted image 20240507181344.png]]

## 内存管理与保护

### 物理内存保护（Physical Memory Protection, PMP）

![[Pasted image 20240507182743.png]]

### 虚拟内存（Virtual Memory）

![[Pasted image 20240507182757.png]]

## 异常和中断

![[Pasted image 20240507183027.png]]