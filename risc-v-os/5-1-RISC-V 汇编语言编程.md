---
aliases: 
tags:
  - risc-v
  - os
  - assembly
summary: 
create time: 2024-05-07
modify time: 2024-05-09
---
![[Pasted image 20240509150349.png]]
i

## 语法

### label

GNU汇编中，任何以冒号结尾的标识符都被认为是一个标号（label）。

### operation

可以有以下多种类型：

#### instruction（指令）

直接对应二进制机器指令的字符串。

#### pseudo-instruction（伪指令）

为了提高编写代码的效率，可以用一条伪指令指示汇编器产生多条实际指令（instruction）。

#### directive（指令/伪操作）

通过类似指令的形式（以 `.` 开头），通知汇编一如何控制代码的产生等，不对应具体的指令。

#### macro（汇编器宏）

汇编器规定的语法，汇编器预处理，采用 `.macro`/`.endm` 自定义的宏。

### comment

`#` ： 常用方式，行处理。 

## 操作对象

### 寄存器

![[Pasted image 20240509192818.png]]

![[Pasted image 20240509193315.png]]

### 内存

![[Pasted image 20240509192827.png]]

## RISC-V汇编编码格式

### 指令长度

$$ILEN1 = 32 bits(RV32I)$$

### 指令对齐

$$ IALIGN = 32bits(RV32I)$$

### 指令的域

一条指令中的不同位的区间的含义。

![[Pasted image 20240509194107.png]]

- funct7/funct3 和 opcode 一起决定最终的指令类型
- 指令在内存中按照 ==小端序== 排列

![[Pasted image 20240509194749.png]]

### 主机字节序（HBO-Host Byte Order）

![[Pasted image 20240509195317.png]]

### 6种指令格式（format）

![[Pasted image 20240509195536.png]]

### 指令分类

![[Pasted image 20240509195816.png]]

### 伪指令

![[Pasted image 20240509200007.png]]

## 指令寻址模式

![[Pasted image 20240510171416.png]]