---
aliases: 
tags:
  - risc-v
  - interrupt
  - exception
  - os
summary: 
create time: 2024-05-13
modify time:
---
## RISC-V 中断（interrupt）分类

![[Pasted image 20240513160554.png]]

1. 本地中断（Local Interrupt）
	1. 软中断（Software Interrupt）
	2. 定时器中断（Timer Interrupt）
2. 全局中断（Global Interrupt）
	1. 外部中断（Externel Interrupt）

### 处理器和中断链接

![[Pasted image 20240513160520.png]]

## RISC-V 中断编程中涉及的寄存器

![[Pasted image 20240513160627.png]]

mstatus 寄存器中的 MIE 为全局中断控制。

### mie(Machine Interrupt Enable)

![[Pasted image 20240513160705.png]]

### mip(Machine Interrupt Pending)

![[Pasted image 20240513160736.png]]

## RICS-V 中断处理流程

1. 关中断
2. 保存现场
3. 跳转中断处理函数
4. 执行中断处理函数
5. 恢复现场
6. 返回

![[Pasted image 20240514113100.png]]

![[Pasted image 20240514113352.png]]

## PLIC 中断相关外设介绍

PLIC(Platform-Level Interrupt Controller)：汇聚和转换不同外部设备产生的外部中断源，控制不同中断源的中断优先级。

![[Pasted image 20240514113835.png]]

### PLIC Interrupt Source

UART 中断号：10

![[Pasted image 20240514113905.png]]

### PLIC编程接口-寄存器

![[Pasted image 20240514114000.png]]

#### 中断优先级配置——Priority寄存器

![[Pasted image 20240514114239.png]]

#### 判断中断源是否发生——Pending

![[Pasted image 20240514114405.png]]

#### 控制中断源的开启与关闭——Enable

![[Pasted image 20240514115236.png]]

#### 设置hart中断优先级的域值

![[Pasted image 20240514115408.png]]

#### Claim/Complete

![[Pasted image 20240514115616.png]]

#### PLIC 操作流程

![[Pasted image 20240514120043.png]]

## 中断方式 UART 实现输入

### PLIC初始化

![[Pasted image 20240514120331.png]]

### 中断判断函数

![[Pasted image 20240514120529.png]]

### 中断处理函数

![[Pasted image 20240514120634.png]]