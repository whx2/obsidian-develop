---
aliases: 
tags:
  - stm32
  - SysTick
summary: 
create time: 2024-03-04
modify time: 2024-03-05
---
## SysTick 简介

系统定时器，24bits，只能递减，存在于内核，嵌套在 NVIC 中，所有 Cortex-M 内核的单片机都具有这个定时器。

## 工作原理

![[Pasted image 20240305161810.png]]

## 寄存器描述

![[Pasted image 20240305161921.png]]

## SysTick 定时时间计算

1. `t` : 一个计数循环的时间，与 `reload` （初始值） 和 `Clock` （时钟频率） 有关。
2. `CLK` : 72MHz || 9MHz，由 `CTRL` 控制
3. `Reload` : 24bit，用户配置

![[Pasted image 20240305162844.png]]

## 中断优先级配置

使用4和二进制位表示优先级，均有优先级分组，组间为抢占式优先级，组内为子优先级。

### 外设中断优先级配置

通过配置外设 `Nested vectored interrupt control(NVIC)` 的 `Interrupt priority registers (NVIC_IPRx)` 寄存器配置优先级。

`nvic->iprx`

### 内核外设中断优先级

通过配置外设 `System control block` 的 `System handler priority registers(SHPRx)` 寄存器配置优先级。

`scb->shprx`

### 中断优先级比较

![[Pasted image 20240305164754.png]]