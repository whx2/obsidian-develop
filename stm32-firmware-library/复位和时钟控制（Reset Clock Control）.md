---
aliases: 
tags:
  - stm32
  - RCC
summary: 
create time: 2024-03-02
modify time: 2024-03-02
---
## 时钟树主系统时钟

不同的外设需要不同的时钟驱动外设工作。

基础时钟的初始化配置在函数 `SystemInit` 中通过配置 ==RCC 控制寄存器== 实现。

![[Pasted image 20240302180759.png]]

基本时钟源（图中绿色箭头指出）：

1. HSI高速内部时钟，RC振荡器，8MHz。
2. HSE高速外部时钟，石英/陶瓷谐振器，8MHz。
3. LSI低速内部时钟，RC振荡器，40kHz。
4. LSE低速外部时钟，RTC石英晶振，32.768kHz。
5. 除了上述基本时钟源，还有num3中的PLL锁相环倍频时钟源，它是将HSI和HSE倍频后输出。

注：内部指的是片上外设，外部指的是芯片外部。

## 系统时钟配置

系统时钟有三个来源，PLLCLK、HSE、HSI。

正常情况下，时钟配置是在 `system_stm32f10x.c` 中完成，这里的的时钟配置是直接控制寄存器完成的。

在 `stm32f10x_rcc.c` 中定义了关于时钟配置的库函数，此时未用。

打开 `system_stm32f10x.c` 找到 `void SystemInit (void)` ，再找到 `SetSysClock()` 并查看定义，

定义中可知是通过在 `system_stm32f10x.c` 中宏定义 `SYSCLK_FREQ_72MHz` 选择系统时钟配置函数 `SetSysClockTo72()` ，即72MHz的系统时钟就是在此函数中配置的，函数如下：（HSE(不分频)->PLLCLK（9倍频）->72MHz系统时钟）

### HSE时钟

High Speed External Clock signal，高速的外部时钟。

#### 晶振电路

无源晶振（4～16MHz），通常使用8MHz。

![[Pasted image 20240302161726.png]]

[野火_指南者原理图_2020-05-06](obsidian://open?vault=obsidian-develop&file=stm32-firmware-library%2Fattachments%2F%E9%87%8E%E7%81%AB_%E6%8C%87%E5%8D%97%E8%80%85%E5%8E%9F%E7%90%86%E5%9B%BE_2020-05-06.pdf)

#### 控制

`RCC_CR`  时钟控制寄存器的位16：`HSEON` 控制。

### HSI 时钟

Low Speed Internal Clock signal，高速的内部时钟。

#### 来源

芯片内部，大小8MHz，当 HSE 故障时，系统时钟会自动切换到 HSI ，直到 HSE 启动成功。

#### 控制

`RCC_CR`  时钟控制寄存器的位16：`HSION` 控制。

### 锁相环时钟 PLLCLK

#### 来源

（HSI/2 || HSE）经过倍频所得。

#### 控制

CFGR：PLLXTPRE、PLLMUL

![[Pasted image 20240302162922.png]]

### 系统时钟

![[Pasted image 20240302163029.png]]

### HCLK 时钟

![[Pasted image 20240302163241.png]]

### PCLK1 时钟

![[Pasted image 20240302163358.png]]

### PCLK2 时钟

![[Pasted image 20240302164239.png]]

### RTC 时钟

![[Pasted image 20240302164302.png]]

### 独立看门狗时钟

![[Pasted image 20240302164447.png]]

![[Pasted image 20240302164435.png]]

## MCO 时钟输出

可以通过对 PA8 外接示波器检测内部时钟配置情况。

![[Pasted image 20240302164537.png]]

## CSS 时钟安全系统

![[Pasted image 20240302165335.png]]