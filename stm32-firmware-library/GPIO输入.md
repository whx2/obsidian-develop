---
aliases: 
tags:
  - stm32
  - GPIO
summary: 
create time: 2024-03-01
modify time: 2024-03-01
---
## 电路原理图

![[Pasted image 20240301153926.png]]

1. R7/R11 限流电阻
2. C6/C15 硬件消抖电容