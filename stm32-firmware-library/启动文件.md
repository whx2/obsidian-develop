---
aliases: 
tags:
  - stm32
summary: 
create time: 2024-03-01
modify time: 2024-03-01
---
## 功能

### 初始化设置堆栈指针SP

![[Pasted image 20240301175426.png]]

### 初始化程序计数器PC

PC 值为 `Reset handler` 复位程序，声明外部函数编译器C库函数`__main`和系统时钟初始化函数`SystemInit` ，加载并跳转执行 `SystemInit` 后返回 ，加载并跳转执行`__main` 后不返回。

### 初始化中断向量表

存储在flash的0地址处，全局内存映射地址 `0x08000000` 处。

#### STM32F10xxx产品(小容量、中容量和大容量)的向量表

包括内核中断，外设中断，详见[1-STM32F10x-中文参考手册](obsidian://open?vault=obsidian-develop&file=stm32-firmware-library%2Fattachments%2F1-STM32F10x-%E4%B8%AD%E6%96%87%E5%8F%82%E8%80%83%E6%89%8B%E5%86%8C.pdf)9中断和事件中断向量表。

### 配置系统时钟

配置挂载开发板的上的片外SRAM作为数据内存（可选）

### 跳转到C库函数`__main`（最终调用用户程序主函数`main`）

系统C库函数 `__main`，实现堆栈初始化。