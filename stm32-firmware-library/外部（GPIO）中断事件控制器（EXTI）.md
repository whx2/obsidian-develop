---
aliases: 
tags:
  - stm32
  - GPIO
  - EXTI
summary: 
create time: 2024-03-03
modify time: 2024-03-03
---
EXTI(External interrupt / event controller)，外部事件（GPIO输入电平变化）触发EXTI，EXTI触发NVIC。

![[Pasted image 20240303190810.png]]

## 外部中断输入源

![[Pasted image 20240303192600.png]]

## 外部中断事件寄存器配置

### EXTI 寄存器配置

1. `EXTI——Line` : 用于产生 中断/事件 线， 如果是 GPIO 外部中断，为指定 GPIO 引脚编号，同时需要配置 AFIO 设备 `AFIO_EXITCR1` 选择具体的 GPIO 设备。
2. `EXTI_Mode` : 中断模式、事件模式
3. `EXIT_Trigger` : 上升沿触发、下降沿触发、上升下降沿触发
4. `EXTI_LindCmd` : EXTI 状态（ENABLE、DISABLE）

### AFIO 寄存器配置

如果需要配置 GPIO 中断，需要指定具体的 GPIO 设备，通过配置 `AFIO_EXITCR1` 选择具体的 GPIO 设备。