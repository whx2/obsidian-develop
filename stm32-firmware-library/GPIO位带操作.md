---
aliases: 
tags:
  - stm32
  - GPIO
summary: 
create time: 2024-03-01
modify time: 2024-03-01
---
[CM3权威指南CnR2](obsidian://open?vault=obsidian-develop&file=stm32-firmware-library%2Fattachments%2FCM3%E6%9D%83%E5%A8%81%E6%8C%87%E5%8D%97CnR2.pdf)

支持了位带操作后，可以使用普通的加载/存储指令来对单一的比特进行读写。在 CM3 中，有两个区中实现了位带。其中一个是 SRAM 区的最低 1MB 范围，第二个则是片内外设区的最低 1MB 范围。这两个位带中的地址除了可以像普通的 RAM 一样使用外，它们还都有自己的“位带别名区”，位带别名区把每个比特膨胀成一个 32 位的字。当你通过位带别名区访问这些字时，就可以达到访问原始比特的目的。

![[Pasted image 20240301163056.png]]

## 地址转换

![[Pasted image 20240301163537.png]]

## 统一地址

![[Pasted image 20240301163657.png]]

## 代码实现

