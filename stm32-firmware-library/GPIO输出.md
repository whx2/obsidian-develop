---
aliases: 
tags:
  - stm32
  - GPIO
summary: 
create time: 2024-03-01
modify time: 2024-03-01
---
## bsp (board support package) 板级支持包

针对特定开发板的特性的支持包。

## 代码实现

### `bsp_led.h`

```c
#ifndef __BSP_LED_H__
#define __BSP_LED_H__

#include "stm32f10x.h"

#define LED_G_GPIO_PIN   GPIO_Pin_0
#define LED_G_GPIO_PORT  GPIOB
#define LED_G_GPIO_CLK   RCC_APB2Periph_GPIOB

#endif // __BSP_LED_H__
```

### `bsp_led.c`

```c
#include "bsp_led.h"

void LED_GPIO_Config(void) {
	
}
```

### `main.c`

```c
#include "stm32f10x.h"

```
