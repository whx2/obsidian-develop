---
aliases: 
tags:
  - ftp
  - server
  - linux
summary: 
create time: 2024-01-02
modify time:
---
## 工作原理

### 连接类型

控制连接（持续连接）：TCP 21（命令信道），用户发送 FTP 命令
数据连接（按需连接）：TCP 20（数据信道），用于上传下载数据

### 工作模式

#### Active Mode

客户端开放端口大于 1024

![[Pasted image 20240102191821.png]]

#### Passive Mode

服务器端临时端口大于 1023 小于 65535

![[Pasted image 20240102201100.png]]

### 传输模式

#### Binary 模式

不对数据进行任何处理，适合可执行文件、压缩文件、图片

#### ASCII 模式

文本传输，自动适应目标操作系统的回车符

## Vsftpd

### 配置文件

1. `/etc/vsftp/vsftp.conf`，主配置文件
2. `/etc/pam.d/vsftpd`，vsftpd 的 Pluggable Authentication Modules(PAM) 配置文件
3. `/etc/vsftpd/ftpusers`，vsftpd 和黑名单，默认包括 root
4. `/etc/vsftpd/user_list`，vsftpd 用户配置

### 验证方式

#### 匿名用户

#### 本地用户

#### 虚拟用户
