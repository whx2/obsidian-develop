---
aliases: 
tags:
  - nginx
  - web
  - server
  - linux
summary: 
create time: 2024-01-01
modify time:
---
## 基本概念

### What Nginx can do ?

高性能，高并发 HTTP 和反向代理服务器。

### 反向代理

#### 正向代理

#### 反向代理

### 负载均衡

### 动静分离

动态页面和静态页面由不同的服务器来解析。

## 使用及配置

### Install Nginx on linux

### Nginx commands

### Nginx configuration

在使用 `apt` 包管理器安装 Nginx 时，配置文件通常存放在 `/etc/nginx` 目录下。Nginx 的主要配置文件是 `nginx.conf`，而网站的具体配置文件则存放在 `/etc/nginx/sites-available` 目录中，通常有一个默认配置文件叫做 `default`。

以下是这些主要配置文件的路径：

1. **主配置文件：**
    
    `/etc/nginx/nginx.conf`
    
    这个文件包含了全局的配置信息，如 worker 进程数、日志路径等。
    
2. **默认网站配置文件：**
    
    `/etc/nginx/sites-available/default`
    
    默认的网站配置文件，如果没有手动创建其他网站的配置文件，Nginx 会使用这个文件来配置默认的站点。
    
3. **网站配置文件存放目录：**
    
    `/etc/nginx/sites-available/`
    
    该目录存放了每个虚拟主机（站点）的配置文件。这些文件通常以站点的域名或项目的名称命名。
    
4. **启用的网站配置文件符号链接：**
    
    `/etc/nginx/sites-enabled/`
    
    这个目录通常包含对 `sites-available` 目录中配置文件的符号链接。只有启用的站点配置文件才会在 Nginx 中生效。

## 实例

### 反向代理

### 负载均衡

#### 负载均衡策略

##### 轮询（Round Robin）

- `round-robin` 是默认的负载均衡算法。
- 每个请求按顺序分发给后端服务器，循环往复。

```nginx
upstream backend {
    server backend1.example.com;
    server backend2.example.com;
    server backend3.example.com;
}

server {
    location / {
        proxy_pass http://backend;
    }
}
```

##### 最小连接数（Least Connections)

- 请求将被分发到当前活跃连接数最少的服务器。
- `least_conn` 指令用于启用最小连接数负载均衡算法。

```nginx
upstream backend {
    least_conn;
    server backend1.example.com;
    server backend2.example.com;
    server backend3.example.com;
}

server {
    location / {
        proxy_pass http://backend;
    }
}
```

##### IP 哈希（IP Hash）

- 根据客户端 IP 地址的哈希值选择一个后端服务器。
- 适用于需要保持同一客户端始终连接到同一后端服务器的场景。

```nginx
upstream backend {
    ip_hash;
    server backend1.example.com;
    server backend2.example.com;
    server backend3.example.com;
}

server {
    location / {
        proxy_pass http://backend;
    }
}
```

##### 加权轮询（Weighted Round Robin）

- 每个后端服务器分配一个权重，按照权重分发请求。
- `weight` 指令用于设置每个后端服务器的权重。

```nginx
upstream backend {
    server backend1.example.com weight=3;
    server backend2.example.com weight=1;
    server backend3.example.com weight=2;
}

server {
    location / {
        proxy_pass http://backend;
    }
}
```

##### 加权最小连接数（Weighted Least Connections）

- 结合了权重和最小连接数算法，按照权重和连接数来分发请求。

```nginx
upstream backend {
    least_conn;
    server backend1.example.com weight=3;
    server backend2.example.com weight=1;
    server backend3.example.com weight=2;
}

server {
    location / {
        proxy_pass http://backend;
    }
}
```

### 动静分离

### 高可用集群

## 原理