---
aliases: 
tags: 
summary: 
create time: 2024-01-01
modify time:
---
UDP 端口68用于 DHCP（Dynamic Host Configuration Protocol）服务的通信。DHCP 是一种网络协议，它允许计算机设备自动获取 IP 地址和其他网络配置信息，而无需管理员手动分配这些信息。这是在网络中动态分配 IP 地址的常见方式之一。

具体而言，端口68是DHCP客户端用于与DHCP服务器进行通信的默认端口。当设备启动时，DHCP客户端通过在局域网内广播DHCP请求，寻找可用的DHCP服务器。DHCP服务器监听端口67来接收客户端的请求，并向客户端提供IP地址、子网掩码、网关等网络配置信息。DHCP客户端使用端口68来接收来自DHCP服务器的响应。

总的来说，UDP端口68用于DHCP客户端向DHCP服务器请求网络配置信息，以实现网络自动配置。